#!/usr/bin/env python3

import string


TRANSLIT_URL_DICT = {
        u'а': u'a',
        u'б': u'b',
        u'в': u'v',
        u'г': u'g',
        u'д': u'd',
        u'е': u'e',
        u'ё': u'yo',
        u'ж': u'zh',
        u'з': u'z',
        u'и': u'i',
        u'й': u'j',
        u'к': u'k',
        u'л': u'l',
        u'м': u'm',
        u'н': u'n',
        u'о': u'o',
        u'п': u'p',
        u'р': u'r',
        u'с': u's',
        u'т': u't',
        u'у': u'u',
        u'ф': u'f',
        u'ц': u'c',
        u'ч': u'ch',
        u'ш': u'sh',
        u'щ': u'shch',
        u'ы': u'y',
        u'э': u'e',
        u'ю': u'yu',
        u'я': u'ya'
        }


def translit(text: str) -> str:
    _translit_list = []
    _prev = ''
    for symbol in text:
        # print(u'Символ', symbol, 'capital =', symbol.isupper())
        # не буквы
        if (not symbol.isalpha()) or (symbol in string.ascii_letters):
            _prev = ''
            _translit_list.append(symbol)
            continue
        # ь и ъ знаки
        if symbol in ('ъ', 'ь'):
            _prev = ''
            continue

        # основные замены
        capital = symbol.isupper()
        tr_sym = TRANSLIT_URL_DICT.get(symbol.lower(), None)
        if symbol.lower() == 'х':  # буква 'х'
            tr_sym = 'kh' if _prev in 'cseh' else 'h'
        if tr_sym is None:
            # print(u'Символ - непонятная хрень')
            _prev = ''
            continue
        _prev = tr_sym[-1]
        if capital:
            tr_sym = tr_sym.capitalize()
        _translit_list.append(tr_sym)

    return ''.join(_translit_list)


if __name__ == '__main__':
    __author__ = 'master by Vint'
    __title__ = '--- translit Yandex ---'
    __version__ = '0.1.3'
    __copyright__ = 'Copyright 2019 ©  bitbucket.org/Vintets'

    _t1 = 'Я уже устал повторять, зачем вообще это нужно. В следующий раз... Test!?&-_()"'
    _t2 = 'цхенвал схема мех шхуна бахча кх зх цх сх ех жх'
    _t3 = 'Ё\tй\tц\tщ\tъыь\tэ\tю\tя'
    _t4 = 'кх\tзх\tцх\tсх\tех\tжх'
    print(f'{_t1}\n{translit(_t1)}\n')
    print(f'{_t2}\n{translit(_t2)}\n')
    print(f'{_t3}\n{translit(_t3)}\n')
    print(f'{_t4}\n{translit(_t4)}\n')

    input('---------------   END   ---------------')


'''
u'Ё й ц щ  ъыь э ю я  кх зх цх сх ех жх'

http://www.translityandex.ru/
yo-y-c-shch--y-e-yu-ya--kh-zh-ch-sh-eh-zhh

http://translit-online.ru/yandex.html
yo-j-c-shch-y-eh-yu-ya-kkh-zkh-ckh-skh-ekh-zhkh

https://translitonline.com/dlya-seo/
yo-j-c-shch-y-e-yu-ya-kh-zh-ckh-skh-ekh-zhkh
'''
