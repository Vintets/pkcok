import re
from pathlib import Path
import hashlib
import aiofiles
from aiofiles.os import remove
import configs.config as config
from storage.translit import translit


async def read(received: dict) -> tuple[bool, str]:
    """Handler received GET command."""

    filename = get_filename(received['name'])
    try:
        async with aiofiles.open(filename, 'r', encoding='utf-8') as fr:
            text = await fr.read()
        return True, text
    except FileNotFoundError:
        return False, ''


async def write(received: dict) -> None:
    """Handler received WRITE command."""

    filename = get_filename(received['name'])
    async with aiofiles.open(filename, 'w', encoding='utf-8') as f:
        await f.write(received['data'])


async def delete(received: dict) -> bool:
    """Handler received DELETE command."""

    status_delete = False
    filename = get_filename(received['name'])
    if filename.exists() and not filename.is_dir():
        await remove(filename)
        status_delete = True
    return status_delete


def get_filename(name: str) -> Path:
    slug = slugify(name)
    md5hash = md5(name)
    return config.PATH_DB / f'{slug}_{md5hash}.txt'


def slugify(string: str) -> str:
    translit_s = translit(string)
    pattern = r'[^\w+]'
    format_str = re.sub(pattern, '_', translit_s.lower())
    while '__' in format_str:
        format_str = format_str.replace('__', '_')
    return format_str


def md5(st: str) -> str:
    return hashlib.md5(st.encode()).hexdigest()
