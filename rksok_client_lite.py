#!/usr/bin/env python3

import sys
import socket
import time
from typing import Callable
from rksok.protocol_rksok import RKSOK
from core.errors import process_critical_exception
import configs.config as config


class Client():
    """RKSOK Client for tests"""

    def __init__(self, host: str, port: int) -> None:
        self.host = host
        self.port = port

    def connection(self) -> socket.socket:
        """Create client socket connection"""

        try:
            conn = socket.create_connection((self.host, self.port))
        except ConnectionRefusedError:
            process_critical_exception('Не могу подключиться к указанному серверу')
        return conn

    def send_test(self, message: str, sleep: int = 0) -> None:
        """Send test requests on server PKCOK"""

        conn = self.connection()
        if sleep:
            conn.sendall(message[:35].encode(RKSOK.encoding))
            time.sleep(2)
            conn.sendall(message[35:].encode(RKSOK.encoding))
        else:
            conn.sendall(message.encode(RKSOK.encoding))

        response = self.receiver(conn)
        print('response:\t', response)

        conn.shutdown(socket.SHUT_RDWR)
        conn.close()

    def receiver(self, client_socket: Callable) -> bytes:
        """Receiver data from socket"""

        chunks = []
        while True:
            chunk = client_socket.recv(1024)
            if not chunk:
                # raise RuntimeError("Соединение сокета нарушено")
                break
            chunks.append(chunk)
            if chunk.endswith(RKSOK.end_of_request.encode(RKSOK.encoding)):
                break
        return b''.join(chunks).decode(RKSOK.encoding)


if __name__ == '__main__':
    local_ = True if len(sys.argv) > 1 and sys.argv[1] == '--local' else False
    server_name = config.SERVER_LOCAL if local_ else config.SERVER_PRODUCTION
    client = Client(**server_name)

    message1 = 'ЗОПИШИ Иван Хмурый РКСОК/1.0\r\n89012345678 — мобильный\r\n02 — рабочий\r\n\r\n'
    message2 = 'ОТДОВАЙ Иван Хмурый РКСОК/1.0\r\n\r\n'
    message3 = 'УДОЛИ Иван Хмурый РКСОК/1.0\r\n\r\n'

    message4 = 'НИПОНЯЛ РКСОК/1.0\r\n\r\n'
    message5 = 'МОЖНА РКСОК/1.0\r\n\r\n'
    message6 = 'НИЛЬЗЯ РКСОК/1.0\r\nУже едем\r\n\r\n'

    message7 = 'ОТДОВАЙ Поликарп Поликарпович Поликарпов РКСОК/1.0\r\n\r\n'
    message8 = 'ОТДОВАЙ Саша Пушкин РКСОК/1.0\r\n\r\n'

    client.send_test(message1, sleep=2)
    client.send_test(message2)
    # client.send_test(message3)
    # client.send_test(message8)
