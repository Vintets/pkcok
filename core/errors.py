from typing import Optional


class CreateServerError(Exception):
    """Error create RKSOK server."""
    def __init__(self, err):
        self.msg = f'Ошибка создания сервера. {err}'

    def __str__(self):
        return self.msg


class ConnectionError(Exception):
    """Error connection socket"""
    def __init__(self, text: Optional[str] = None):
        self.msg = text if text is not None else 'Подключение разорвано'

    def __str__(self):
        return self.msg


def process_critical_exception(message: Optional[str] = None):
    """Prints message, describing critical situation, and exit"""

    if str is not None:
        print(message)
    exit(1)
