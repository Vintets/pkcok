#!/usr/bin/env python3

import sys
from pathlib import Path
from loguru import logger


PATH_LOG = Path('logs')
FILENAME_LOG_MAIN = PATH_LOG / 'rksok_main_{time:YYYY-MM-DD}.log'
FILENAME_LOG_ERR = PATH_LOG / 'rksok_error_{time:YYYY-MM-DD}.log'
FILENAME_LOG_INCOMING = PATH_LOG / 'rksok_incoming_{time:YYYY-MM-DD}.log'


def incoming_message(ip: str, client_data: str) -> None:
    """Create message from logger INCOMING"""

    return f'Получили данные по сокету от {ip!r}:\n{"-"*15}\n{client_data!r}\n{"-"*15}'


# исправление цвета INFO на windows с серой консолью
logger.level('INFO', color='<light-white>')

# добавляем свои уровни 'FAIL' и 'INCOMING'
logger.level('FAIL', no=27, color='<light-magenta>', icon='@')
logger.level('INCOMING', no=23, color='<light-blue>', icon='<<==')

# удаляем начальный логгер и создаём свой базовый логгер с уровнем default LOGURU_LEVEL 'DEBUG'
logger.remove()
logger.add(sys.stdout, format='<green>{time:YYYY-MM-DD HH:mm:ss.SSS}</green> | <lvl>{level: <8}</lvl> | <cyan>{line}</cyan>- <lvl>{message}</lvl>')

# добавляем логгеры с дефолтным форматированием для вывода в файлы
logger.add(FILENAME_LOG_MAIN, filter=lambda record: record['level'].no <= 30, rotation='00:00', compression='zip')
logger.add(FILENAME_LOG_ERR, filter=lambda record: record['level'].no >= 30, rotation='00:00', compression='zip')
logger.add(FILENAME_LOG_INCOMING, filter=lambda record: record['level'].name == 'INCOMING', rotation='00:00', compression='zip')


def exemples():
    logger.trace('Hello, World (trace)!')
    logger.debug('Hello, World (debug)!')
    logger.info('Hello, World (info)!')
    logger.success('Hello, World (success)!')
    logger.warning('Hello, World (warning)!')
    logger.error('Hello, World (error)!')
    logger.critical('Hello, World (critical)!')
    logger.log('FAIL', 'No data recorded!')
    print()

    # logger.success('Operation completed Ok!')
    logger.log('INCOMING', incoming_message(ip='192.168.0.200', client_data='ОТДОВАЙ Саша Пушкин РКСОК/1.0\r\n\r\n'))


def create_dir_log() -> None:
    if not (PATH_LOG.exists() and PATH_LOG.is_dir()):
        PATH_LOG.mkdir()


create_dir_log()

if __name__ == '__main__':
    exemples()
