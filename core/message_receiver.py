import asyncio
import core.errors as err
from rksok.protocol_rksok import RKSOK


async def receiver(reader: asyncio.StreamReader) -> bytes:
    """Receiver data from socket"""

    # message = await reader.readuntil(separator=RKSOK.end_of_request.encode(RKSOK.encoding))
    message = b''
    while True:
        chunk = await reader.read(1024)
        if not chunk:
            raise err.ConnectionError()
        message += chunk
        if message.endswith(RKSOK.end_of_request.encode(RKSOK.encoding)):
            break
    return message.decode(RKSOK.encoding)
