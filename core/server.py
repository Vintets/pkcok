#!/usr/bin/env python3

import asyncio
import socket
import configs.config as config
import core.errors as err
from core.log import logger, incoming_message
from rksok.protocol_rksok import RKSOK, RequestVerb, ClientResponseStatus
from rksok.supervisory import Supervisory
from core.message_receiver import receiver
import storage.store as storage


async def actions_received(received_data_from_client: str, rksok: RKSOK) -> str:
    """Main actions in correct client request"""

    if received_data_from_client['command'] == RequestVerb.GET:
        status_read, text = await storage.read(received_data_from_client)
        if status_read:
            reply_message = rksok.create_message(ClientResponseStatus.OK, body_request=text)
        else:
            reply_message = rksok.create_message(ClientResponseStatus.NOT_FOUND)
    elif received_data_from_client['command'] == RequestVerb.WRITE:
        await storage.write(received_data_from_client)
        reply_message = rksok.create_message(ClientResponseStatus.OK)
    elif received_data_from_client['command'] == RequestVerb.DELETE:
        if await storage.delete(received_data_from_client):
            reply_message = rksok.create_message(ClientResponseStatus.OK)
        else:
            reply_message = rksok.create_message(ClientResponseStatus.NOT_FOUND)
    return reply_message


async def processing_client_request(client_raw_data: str) -> str:
    """Processing of data received from the client."""

    rksok = RKSOK()
    if rksok.validate_protocol(client_raw_data):
        kgb = Supervisory(client_raw_data)
        await kgb.send_for_approval()
        if kgb.check_response_supervisory():
            received_data_from_client = rksok.parsing_data_from_client(client_raw_data)
            response_to_client = await actions_received(received_data_from_client, rksok)
        else:
            response_to_client = kgb.response_raw_supervisory
    else:
        response_to_client = rksok.create_message(ClientResponseStatus.INCORRECT_REQUEST)
    return response_to_client


async def handler_client(reader: asyncio.StreamReader, writer: asyncio.StreamWriter) -> None:
    """Client connection handler."""

    print(f'\n{"*" * 100}')
    try:
        addr = writer.get_extra_info('peername')
        logger.info(f'Подключился клиент: {addr}')
        client_raw_data = await asyncio.wait_for(receiver(reader), timeout=config.TIMEOUT_TO_RECEIVE)
        logger.log('INCOMING', incoming_message(ip=addr, client_data=client_raw_data))

        response_to_client = await processing_client_request(client_raw_data)

        logger.info(f'Отвечаем: {response_to_client!r}')
        writer.write(response_to_client.encode(RKSOK.encoding))
        await writer.drain()
    except (OSError, ConnectionResetError, err.ConnectionError) as e:
        logger.log('FAIL', f'Соединение сокета нарушено {e}')
    except (asyncio.TimeoutError, RuntimeError):
        logger.log('FAIL', f'TimeoutError incoming connection from client')
    finally:
        logger.info('Connection closed')
        print()
        writer.close()


async def run_server(host: str, port: int) -> None:
    try:
        server = await asyncio.start_server(handler_client, host, port)
    except (socket.gaierror, OverflowError, TypeError) as e:
        raise err.CreateServerError(e)  # from None

    addrs = ', '.join(str(sock.getsockname()) for sock in server.sockets)
    logger.success(f'Serving on {addrs}')

    async with server:
        await server.serve_forever()


def main(host: str, port: int) -> None:
    try:
        asyncio.run(run_server(host, port))
    except err.CreateServerError as e:
        logger.critical(e)  # __str__()
        exit(1)
    except KeyboardInterrupt:
        logger.info('Server stopped.')
