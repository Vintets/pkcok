import asyncio
import configs.config as config
import core.errors as err
from core.log import logger
from core.message_receiver import receiver
from rksok.protocol_rksok import RKSOK, RequestPermission, SupervisoryResponseStatus


class Supervisory:
    """Inspection by supervising organization"""

    def __init__(self, client_raw_data: str) -> None:
        self._client_raw_data = client_raw_data
        self._response_raw_supervisory = None
        self._rksok = RKSOK()

    @property
    def response_raw_supervisory(self) -> str:
        return self._response_raw_supervisory

    async def send_for_approval(self) -> None:
        """Send the accepted request for approval to the supervisory"""

        message = self.create_message_for_approval_in_supervisor()
        reader, writer = await asyncio.open_connection(
                                        config.INSPECTION_SERVER['host'],
                                        config.INSPECTION_SERVER['port']
                                        )

        writer.write(message.encode())
        await writer.drain()

        try:
            self._response_raw_supervisory = await asyncio.wait_for(receiver(reader), timeout=config.TIMEOUT_TO_RECEIVE)
            logger.info(f'response_supervisory:\t{self._response_raw_supervisory!r}')
        except err.ConnectionError as e:
            logger.error(e)
        except (asyncio.TimeoutError, RuntimeError):
            logger.error('Timeout connection supervisory!')
        finally:
            writer.close()
            await writer.wait_closed()

    def create_message_for_approval_in_supervisor(self) -> str:
        return self._rksok.create_message(RequestPermission.REQUEST, self._client_raw_data)

    def check_response_supervisory(self) -> bool:
        """Confirmation of the response for compliance with the party line"""

        if self._rksok.validate_protocol(self._response_raw_supervisory):
            if self._rksok.parsing_received_data_supervisory(self._response_raw_supervisory)['command'] == SupervisoryResponseStatus.APPROVED:
                return True
        return False
