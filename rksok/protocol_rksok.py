from enum import Enum
from typing import Union, Optional


class RequestVerb(Enum):
    """Verbs specified in RKSOK specs for requests"""

    GET = 'ОТДОВАЙ'
    DELETE = 'УДОЛИ'
    WRITE = 'ЗОПИШИ'


class ClientResponseStatus(Enum):
    """Response statuses client specified in RKSOK specs for responses"""

    OK = 'НОРМАЛДЫКС'
    NOT_FOUND = 'НИНАШОЛ'
    INCORRECT_REQUEST = 'НИПОНЯЛ'


class SupervisoryResponseStatus(Enum):
    """Response statuses supervisory specified in RKSOK specs for responses"""

    APPROVED = 'МОЖНА'
    NOT_APPROVED = 'НИЛЬЗЯ'
    INCORRECT_REQUEST = ClientResponseStatus.INCORRECT_REQUEST.value


class RequestPermission(Enum):
    """Request for permission from the inspection authorities """

    REQUEST = 'АМОЖНА?'


class RKSOK:
    """RKSOK Protocol"""

    protocol = 'РКСОК/1.0'
    encoding = 'UTF-8'
    end_of_request = '\r\n\r\n'

    @staticmethod
    def create_message(
            response_status: Union[ClientResponseStatus, RequestPermission],
            body_request: Optional[str] = None
    ) -> str:
        """Creating a message in RKSOK format"""

        if body_request is None:
            message = f'{response_status.value} {RKSOK.protocol}{RKSOK.end_of_request}'
        else:
            message = f'{response_status.value} {RKSOK.protocol}\r\n{body_request}{RKSOK.end_of_request}'
        return message

    def validate_protocol(self, raw_data: str) -> bool:
        """Validation for compliance with the RKSOC protocol."""

        if not raw_data or not raw_data.endswith(RKSOK.end_of_request):
            return False
        received_list = raw_data.split('\r\n')[:-2]
        header = received_list[0].split(' ')
        # print(f'{header=}')

        request_verb = self._get_request_verb(header[0])
        supervisory_status = self._get_response_status_supervisory(header[0])
        if (len(header) < 2 or
                header[-1] != RKSOK.protocol or
                ((request_verb is None) and (supervisory_status is None))):
            return False

        name = ' '.join(header[1: -1])
        if not self._validate_name(request_verb, name):
            return False

        return True

    def parsing_data_from_client(self, client_raw_data: str) -> dict:
        received_list = client_raw_data.split('\r\n')[:-2]
        header = received_list[0].split(' ')
        received = {
            'command': self._get_request_verb(header[0]),
            'name': ' '.join(header[1: -1]),
            'data': '\r\n'.join(received_list[1:])
        }
        return received

    def parsing_received_data_supervisory(self, received_raw_data: str) -> dict:
        received_list = received_raw_data.split('\r\n')[:-2]
        header = received_list[0].split(' ')
        received = {
            'command': self._get_response_status_supervisory(header[0]),
            'name': ' '.join(header[1: -1]),
            'data': received_raw_data
        }
        return received

    def _validate_name(self, request_verb: Optional[RequestVerb], name: str) -> bool:
        """Validation name from requests."""

        if request_verb is not None and (not name or len(name) > 30):
            print('Bad name!')
            return False
        return True

    def _get_request_verb(self, command_verb: str) -> Optional[RequestVerb]:
        """Getting RequestVerb obj or None"""

        for verb_item in RequestVerb:
            if command_verb == verb_item.value:
                return verb_item
        return None

    def _get_response_status_supervisory(self, status: str) -> Optional[SupervisoryResponseStatus]:
        """Getting SupervisoryResponseStatus obj or None"""

        for supervisory_status in SupervisoryResponseStatus:
            if status == supervisory_status.value:
                return supervisory_status
        return None
