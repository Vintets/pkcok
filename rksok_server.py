#!/usr/bin/env python3

import sys
import configs.config as config
import core.server as server


def create_dir_db() -> None:
    if not (config.PATH_DB.exists() and config.PATH_DB.is_dir()):
        config.PATH_DB.mkdir()


if __name__ == '__main__':
    create_dir_db()
    local_ = True if len(sys.argv) > 1 and sys.argv[1] == '--local' else False
    server_name = config.SERVER_LOCAL if local_ else config.SERVER_PRODUCTION
    server.main(**server_name)
