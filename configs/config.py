from pathlib import Path

SERVER_PRODUCTION = {
            # 'hostname': 'rksok.vintets.ru',
            'host': '185.117.153.104',
            'port': 8899
            }

SERVER_LOCAL = {
            'host': '127.0.0.1',
            'port': 8899
            }

INSPECTION_SERVER = {
            'host': 'vragi-vezde.to.digital',
            'port': 51624
            }

PATH_DB = Path('db')
TIMEOUT_TO_RECEIVE = 20.0
